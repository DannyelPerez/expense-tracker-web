import Axios, {AxiosResponse, AxiosError} from 'axios';
import cookie from 'react-cookies';
import {message} from 'antd';
import {configureStore} from '../store';
import {USER_LOG_OUT} from '../globals/actionsTypes';

const storeConfig = configureStore();
const ConfigureAxios = () => {
  Axios.defaults.baseURL =
    'https://expense-tracker--challenge-api.herokuapp.com/';
  let token = cookie.load('token');
  if (token) {
    Axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  }

  setInterceptor();
};

const setInterceptor = () => {
  Axios.interceptors.response.use(success, error);
};

const success = (response: AxiosResponse) => response;
const error = (error: AxiosError) => {
  
  if (error.response) {
    let data = error.response.data.error;
    data.catch = false;

    switch (error.response.status) {
      case 401:
        cookie.remove('token');
        cookie.remove('userId');

        if (storeConfig.getState().Auth.isAuthenticated) {
          message.warning('your session has expired');
        } else {
          message.error(data.message);
        }
        storeConfig.dispatch({type: USER_LOG_OUT});
        Axios.defaults.headers.common['Authorization'] = null;
        data.catch = true;
        return Promise.reject(data);
      case 500:
        message.error('Something Happend');
        data.catch = true;
        return Promise.reject(data);
    }
    data.catch = true;
    return Promise.reject(data);
  } else if (error.request) {
    message.error('Server Error', 5);
  } else {
    message.error('Something happend', 5);
  }
};
export default ConfigureAxios;
