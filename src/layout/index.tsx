import React, {useEffect} from 'react';
import UnauthorizedLayout from './unauthorized';
import MainLayout from './main';
import {AppState} from '../store';
import {connect, ConnectedProps} from 'react-redux';
import cookie from 'react-cookies';
import {setIsAuthenticated, setUserId} from '../views/auth/actions';

const mapStateToProps = (state: AppState) => ({
  isAuthenticated: state.Auth.isAuthenticated,
});

const mapDispatchToProps = {
  setIsAuthenticated: setIsAuthenticated,
  setUserId: setUserId,
};

const connector = connect(mapStateToProps, mapDispatchToProps);

type Props = ConnectedProps<typeof connector>;

const AppLayout = (props: Props) => {
  useEffect(() => {
    let token = cookie.load('token');
    let userId = cookie.load('userId');
    let userStillAuthenticated = token && userId && !props.isAuthenticated;
    props.setIsAuthenticated(userStillAuthenticated);
    if (userId) {
      props.setUserId(userId);
    }
  }, []);
  return props.isAuthenticated ? <MainLayout /> : <UnauthorizedLayout />;
};

export default connector(AppLayout);
