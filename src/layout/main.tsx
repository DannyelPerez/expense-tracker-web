import React from 'react';
import {Layout, Menu} from 'antd';
import {HomeOutlined, ReadOutlined, LogoutOutlined} from '@ant-design/icons';
import {MainRoutes} from '../routes';
import {useLocation, useHistory} from 'react-router-dom';
import {logout} from '../views/auth/actions';
import {connect, ConnectedProps} from 'react-redux';
const {Header, Footer, Content, Sider} = Layout;

const mapDispatchToProps = {
  logout: logout,
};

const connector = connect(null, mapDispatchToProps);

type Props = ConnectedProps<typeof connector>;

const MainLayout = (props: Props) => {
  const location = useLocation();
  const history = useHistory();
  return (
    <Layout className="layout">
      <Sider collapsible>
        <div className="logo" />
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={[location.pathname]}
          selectedKeys={[location.pathname]}>
          <Menu.Item
            key="/"
            icon={<HomeOutlined />}
            onClick={() => history.push('/')}>
            Home
          </Menu.Item>
          <Menu.Item
            key="/reports"
            icon={<ReadOutlined />}
            onClick={() => history.push('/reports')}>
            Reports
          </Menu.Item>
          <Menu.Item
            key="/logout"
            icon={<LogoutOutlined />}
            onClick={() => props.logout()}>
            Log Out
          </Menu.Item>
        </Menu>
      </Sider>

      <Layout className="site-layout">
        <Header
          className="site-layout-sub-header-background"
          style={{padding: 0}}
        />
        <Content className="content">
          <div className="site-layout-background container">
            <MainRoutes />
          </div>
        </Content>
        <Footer style={{textAlign: 'center'}}>Expense Tracker ©2020</Footer>
      </Layout>
    </Layout>
  );
};

export default connector(MainLayout);
