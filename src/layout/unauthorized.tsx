import React from 'react';
import {Layout, Row, Col} from 'antd';
import {UnauthorizedRoutes} from '../routes';

const {Footer, Content} = Layout;

const UnauthorizedLayout = () => {
  return (
    <Layout className="layout unauthorized-layout-background">
      <Layout className="site-layout">
        <Content className="unauthorized-wrapper">
          <Row justify="center">
            <Col span={24}>
              <UnauthorizedRoutes />
            </Col>
          </Row>
        </Content>
        <Footer style={{textAlign: 'center'}}>Expense Tracker ©2020</Footer>
      </Layout>
    </Layout>
  );
};

export default UnauthorizedLayout;
