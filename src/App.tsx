import React, {useEffect} from 'react';
import './App.css';
import AppLayout from './layout';
import ConfigureAxios from './config/axios.config';

const App = () => {
  useEffect(() => {
    ConfigureAxios();
  }, []);
  return (
    <div className="App">
      <AppLayout />
    </div>
  );
};

export default App;
