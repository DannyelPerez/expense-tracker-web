import {combineReducers} from 'redux';
import Home from '../views/home/reducer';
import Auth from '../views/auth/reducer';

export const AppReducer = combineReducers({
  Home,
  Auth,
});
export type AppState = ReturnType<typeof AppReducer>;
