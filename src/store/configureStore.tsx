import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {AppReducer, AppState} from './rootReducers';
import {composeWithDevTools} from 'redux-devtools-extension';

export const configureStore = (state?: AppState) => {
  const enhancers = composeWithDevTools({shouldHotReload: true})(
    applyMiddleware(thunk),
  );
  const store = createStore(AppReducer, state, enhancers);
  return store;
};
