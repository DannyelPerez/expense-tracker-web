import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import login from '../views/auth/login';
import signUp from '../views/auth/signUp';

export const UnauthorizedRoutes = () => {
  return (
    <Switch>
      <Route path="/login" exact component={login} />
      <Route path="/signUp" exact component={signUp} />
      <Redirect exact from="*" to="/login" />
    </Switch>
  );
};
