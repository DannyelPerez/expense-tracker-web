import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import home from '../views/home';
import NotFound from '../views/notFound';
import Reports from '../views/reports';

export const MainRoutes = () => {
  return (
    <Switch>
      <Route path="/" exact component={home} />
      <Route path="/reports" exact component={Reports} />
      <Redirect exact from="/login" to="/" />
      <Route component={NotFound} />
    </Switch>
  );
};
