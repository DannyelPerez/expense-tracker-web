import {
  SetIsOpenAccountModal,
  SetIsOpenExpenseRecordModal,
  SetCategories,
  SetRecordTypes,
  SetAccounts,
  AddAccount,
  UpdateBalance,
} from './home';
import {SetIsAuthenticated, UserLogOut, SetUserId} from './auth';

export * from './constants';

export type ActionTypes =
  | SetIsOpenAccountModal
  | SetIsOpenExpenseRecordModal
  | SetIsAuthenticated
  | UserLogOut
  | SetUserId
  | SetCategories
  | SetRecordTypes
  | SetAccounts
  | AddAccount
  | UpdateBalance;
