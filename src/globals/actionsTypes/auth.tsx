import {SET_IS_AUTHENTICATED, USER_LOG_OUT, SET_USER_ID} from './constants';

export interface SetIsAuthenticated {
  type: typeof SET_IS_AUTHENTICATED;
  payload: boolean;
}

export interface UserLogOut {
  type: typeof USER_LOG_OUT;
}

export interface SetUserId {
  type: typeof SET_USER_ID;
  payload: string;
}
