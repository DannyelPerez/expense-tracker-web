import {
  SET_IS_OPEN_ACCOUNT_MODAL,
  SET_IS_OPEN_EXPENSE_RECORD_MODAL,
  SET_CATEGORIES,
  SET_RECORD_TYPES,
  SET_ACCOUNTS,
  ADD_ACCOUNT,
  UPDATE_BALANCE,
} from './constants';
import {Category, RecordType, AccountBalance, BalanceUpdate} from '..';

export interface SetIsOpenAccountModal {
  type: typeof SET_IS_OPEN_ACCOUNT_MODAL;
  payload: boolean;
}

export interface SetIsOpenExpenseRecordModal {
  type: typeof SET_IS_OPEN_EXPENSE_RECORD_MODAL;
  payload: boolean;
}

export interface SetCategories {
  type: typeof SET_CATEGORIES;
  payload: Category[];
}

export interface SetRecordTypes {
  type: typeof SET_RECORD_TYPES;
  payload: RecordType[];
}

export interface SetAccounts {
  type: typeof SET_ACCOUNTS;
  payload: AccountBalance[];
}

export interface AddAccount {
  type: typeof ADD_ACCOUNT;
  payload: AccountBalance;
}

export interface UpdateBalance {
  type: typeof UPDATE_BALANCE;
  payload: BalanceUpdate;
}
