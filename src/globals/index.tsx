export const passwordRegex = /(?=^.{8,15}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;

export const grid = {
  xs: {
    span: 24,
  },
  sm: {
    span: 12,
  },
  md: {
    span: 8,
  },
  lg: {
    span: 6,
  },
};
export interface Credentials {
  email: string;
  password: string;
}

export interface User {
  email: string;
  password: string;
  name: string;
}

export interface Account {
  id: number;
  name: string;
  description: string;
  userId: number;
}

export interface Category {
  id: number;
  description: string;
}

export interface RecordType {
  id: number;
  description: string;
}

export interface Record {
  id: number;
  accountId: number;
  recordTypeId: number;
  categoryId: number;
  value: number;
  balance: number;
  date: string;
}

export interface AccountRecords extends Account {
  records: Record[];
}

export interface AccountBalance extends Account {
  balance: number;
}

export interface BalanceUpdate {
  accountId: number;
  balance: number;
}

export interface RecordRequest {
  accountId: number;
  from: string;
  to: string;
}
