import React from 'react';
import {Form, Input, Button} from 'antd';
import {MailOutlined, LockOutlined} from '@ant-design/icons';
import {Store} from 'antd/lib/form/interface';
import {passwordRegex, Credentials} from '../../globals';
import {Link} from 'react-router-dom';
import {login} from './actions';
import {connect, ConnectedProps} from 'react-redux';
const mapDispatchToProps = {
  doLogin: login,
};

const connector = connect(null, mapDispatchToProps);

type Props = ConnectedProps<typeof connector>;

const Login = (props: Props) => {
  const onFinish = (formValues: Store) => {
    const credentials = formValues as Credentials;
    props.doLogin(credentials);
  };

  return (
    <Form name="loginForm" className="login-form" onFinish={onFinish}>
      <Form.Item
        name="email"
        rules={[
          {required: true, message: 'Please input your Email!', type: 'email'},
        ]}>
        <Input
          prefix={<MailOutlined className="site-form-item-icon" />}
          placeholder="email"
        />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[
          {
            required: true,
            message: 'Please input your Password!',
            pattern: passwordRegex,
          },
        ]}>
        <Input
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="Password"
        />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button">
          Log in
        </Button>{' '}
        Or{' '}
        <Link to="/signUp">
          <a>register now!</a>
        </Link>
      </Form.Item>
    </Form>
  );
};

export default connector(Login);
