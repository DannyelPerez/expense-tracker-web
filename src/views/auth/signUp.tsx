import React from 'react';
import {Form, Input, Button} from 'antd';
import {UserOutlined, LockOutlined, MailOutlined} from '@ant-design/icons';
import {Store} from 'antd/lib/form/interface';
import {passwordRegex, User} from '../../globals';
import {Link, useHistory} from 'react-router-dom';
import {signUp} from './actions';
import {connect, ConnectedProps} from 'react-redux';

const mapDispatchToProps = {
  signUp: signUp,
};

const connector = connect(null, mapDispatchToProps);

type Props = ConnectedProps<typeof connector>;

const SignUp = (props: Props) => {
  const history = useHistory();
  const onFinish = (formValues: Store) => {
    const {email, name, password} = formValues as User;
    props.signUp({email, name, password}).then(() => {
      history.replace('/');
    });
  };

  return (
    <Form name="signForm" className="login-form" onFinish={onFinish}>
      <Form.Item
        name="name"
        rules={[{required: true, message: 'Please input your Name!'}]}>
        <Input
          prefix={<UserOutlined className="site-form-item-icon" />}
          placeholder="name"
        />
      </Form.Item>
      <Form.Item
        name="email"
        rules={[
          {required: true, message: 'Please input your Email!', type: 'email'},
        ]}>
        <Input
          prefix={<MailOutlined className="site-form-item-icon" />}
          placeholder="email"
        />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[
          {
            required: true,
            message: 'Please input your Password!',
            pattern: passwordRegex,
          },
        ]}
        hasFeedback>
        <Input
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="Password"
        />
      </Form.Item>
      <Form.Item
        name="confirm"
        dependencies={['password']}
        hasFeedback
        rules={[
          {
            required: true,
            message: 'Please confirm your Password!',
          },
          ({getFieldValue}) => ({
            validator(rule, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }
              return Promise.reject('The two passwords do not match!');
            },
          }),
        ]}>
        <Input
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="Password"
        />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button">
          Register
        </Button>{' '}
        Or{' '}
        <Link to="/login">
          <a>log in</a>
        </Link>
      </Form.Item>
    </Form>
  );
};

export default connector(SignUp);
