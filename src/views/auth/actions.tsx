import {Dispatch} from 'redux';
import {User, Credentials} from '../../globals';
import {
  SET_IS_AUTHENTICATED,
  ActionTypes,
  USER_LOG_OUT,
  SET_USER_ID,
} from '../../globals/actionsTypes';
import cookies from 'react-cookies';
import axios from 'axios';
export const setIsAuthenticated = (isAuthenticated: boolean): ActionTypes => {
  return {
    type: SET_IS_AUTHENTICATED,
    payload: isAuthenticated,
  };
};

export const setUserId = (userId: string): ActionTypes => {
  return {
    type: SET_USER_ID,
    payload: userId,
  };
};

export const signUp = (user: User) => async (dispatch: Dispatch) => {
  return axios.post('/signup', user).then(() => {
    dispatch({type: USER_LOG_OUT});
  });
};

export const login = (credentials: Credentials) => async (
  dispatch: Dispatch,
) => {
  return axios.post('/users/login', credentials).then((response) => {
    const {token, userId} = response.data;
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    cookies.save('token', token, {path: '/'});
    cookies.save('userId', userId, {path: '/'});
    dispatch(setIsAuthenticated(true));
    dispatch(setUserId(userId));
  });
};

export const logout = () => async (dispatch: Dispatch) => {
  return axios.post('/users/logout').then(() => {
    axios.defaults.headers.common['Authorization'] = '';
    cookies.remove('token');
    cookies.remove('userId');
    dispatch({type: USER_LOG_OUT});
  });
};
