import {
  SET_IS_AUTHENTICATED,
  USER_LOG_OUT,
  ActionTypes,
  SET_USER_ID,
} from '../../globals/actionsTypes';

type AuthState = {
  isAuthenticated: boolean;
  userId: string;
};

const initialState: AuthState = {
  isAuthenticated: false,
  userId: '',
};

const reducer = (state = initialState, action: ActionTypes): AuthState => {
  switch (action.type) {
    case SET_IS_AUTHENTICATED:
      return {
        ...state,
        isAuthenticated: action.payload,
      };
    case SET_USER_ID:
      return {
        ...state,
        userId: action.payload,
      };
    case USER_LOG_OUT:
      return initialState;
    default:
      return state;
  }
};

export default reducer;
