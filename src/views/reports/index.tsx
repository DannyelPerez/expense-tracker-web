import React, {useEffect, useState} from 'react';
import {AppState} from '../../store';
import {getRecords} from './actions';
import {connect, ConnectedProps} from 'react-redux';

import {Form, Button, DatePicker, Select, Row, Col, Table} from 'antd';
import {Store} from 'antd/lib/form/interface';
import {Record} from '../../globals';
import moment from 'moment';
const {RangePicker} = DatePicker;
const {Option} = Select;

const columns = [
  {
    title: 'Category',
    dataIndex: 'category',
  },
  {
    title: 'Type',
    dataIndex: 'type',
  },
  {
    title: 'Date',
    dataIndex: 'date',
  },
  {
    title: 'Value',
    dataIndex: 'value',
  },
  {
    title: 'Balance',
    dataIndex: 'balance',
  },
];

interface Hash<T> {
  [key: number]: T;
}
interface formValues {
  accountId: number;
  dates: moment.Moment[];
}

interface tableItem {
  key: number;
  category: string;
  type: string;
  value: number;
  balance: number;
  date: string;
}

const mapStateToProps = (state: AppState) => ({
  accounts: state.Home.accounts,
  categories: state.Home.categories,
  recordTypes: state.Home.recordTypes,
});

const mapDispatchToProps = {
  getRecords: getRecords,
};

const connector = connect(mapStateToProps, mapDispatchToProps);

type Props = ConnectedProps<typeof connector>;

const Reports = (props: Props) => {
  const [categories, setCategories] = useState<Hash<string>>({});
  const [recordTypes, setRecordTypes] = useState<Hash<string>>({});
  const [tableItems, setTableItems] = useState<tableItem[]>([]);

  useEffect(() => {
    const categoriesOptions: Hash<string> = {};
    props.categories.forEach(({description, id}) => {
      categoriesOptions[id] = description;
    });
    setCategories(categoriesOptions);
  }, []);

  useEffect(() => {
    const recordTypesOptions: Hash<string> = {};
    props.recordTypes.forEach(({description, id}) => {
      recordTypesOptions[id] = description;
    });
    setRecordTypes(recordTypesOptions);
  }, []);

  const onFinish = (formValues: Store) => {
    const filters = formValues as formValues;
    const {accountId, dates} = filters;
    const [from, to] = dates;

    props
      .getRecords({
        accountId,
        from: from.format('YYYY-MM-DD 00:00:00'),
        to: to.format('YYYY-MM-DD 23:59:59'),
      })
      .then((response) => {
        const {data} = response;
        const newTableItems = (data as Record[]).map(
          ({id, balance, value, recordTypeId, categoryId, date}): tableItem => {
            return {
              balance,
              value,
              key: id,
              category: categories[categoryId],
              type: recordTypes[recordTypeId],
              date: moment(date).format('dd, MMM Do h:mmA'),
            };
          },
        );
        setTableItems(newTableItems);
      });
  };

  return (
    <div style={{padding: 24}}>
      <Row justify="center">
        <Col span={24}>
          <Form layout="inline" onFinish={onFinish}>
            <Form.Item
              name="accountId"
              rules={[{required: true, message: 'Please select Account'}]}>
              <Select placeholder="Select an Account">
                {props.accounts.map(({id, name}) => (
                  <Option value={id} key={id}>
                    {name}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              name="dates"
              rules={[
                {type: 'array', required: true, message: 'Please select time!'},
              ]}>
              <RangePicker />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Generate Report
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Table
            tableLayout="fixed"
            columns={columns}
            dataSource={tableItems}
            pagination={{pageSize: 50}}
            scroll={{y: 240}}
          />
        </Col>
      </Row>
    </div>
  );
};

export default connector(Reports);
