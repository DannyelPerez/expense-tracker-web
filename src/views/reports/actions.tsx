import {RecordRequest} from '../../globals';
import Axios from 'axios';

export const getRecords = (request: RecordRequest) => async () => {
  const filter = {
    order: ['date ASC'],
    where: {
      and: [
        {
          accountId: {
            eq: request.accountId,
          },
        },
        {
          date: {
            between: [request.from, request.to],
          },
        },
      ],
    },
  };

  return Axios.get(
    `/records?filter=${encodeURIComponent(JSON.stringify(filter))}`,
  );
};
