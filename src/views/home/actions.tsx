import {
  SET_IS_OPEN_ACCOUNT_MODAL,
  SET_IS_OPEN_EXPENSE_RECORD_MODAL,
  ActionTypes,
  SET_CATEGORIES,
  SET_RECORD_TYPES,
  ADD_ACCOUNT,
  SET_ACCOUNTS,
  UPDATE_BALANCE,
} from '../../globals/actionsTypes';
import {Dispatch} from 'redux';
import Axios from 'axios';
import {
  Category,
  RecordType,
  Account,
  AccountRecords,
  AccountBalance,
  Record,
  BalanceUpdate,
} from '../../globals';
import {AppState} from '../../store';
import {message} from 'antd';

export const setIsOpenAccountModal = (isOpen: boolean): ActionTypes => {
  return {
    type: SET_IS_OPEN_ACCOUNT_MODAL,
    payload: isOpen,
  };
};

export const setIsOpenExpenseRecordModal = (isOpen: boolean): ActionTypes => {
  return {
    type: SET_IS_OPEN_EXPENSE_RECORD_MODAL,
    payload: isOpen,
  };
};

export const setCategories = (categories: Category[]): ActionTypes => {
  return {
    type: SET_CATEGORIES,
    payload: categories,
  };
};

export const setRecordTypes = (recordTypes: RecordType[]): ActionTypes => {
  return {
    type: SET_RECORD_TYPES,
    payload: recordTypes,
  };
};

export const getCategories = () => async (dispatch: Dispatch) => {
  return Axios.get('/options/categories').then((response) => {
    const {data} = response;
    const categories = data as Category[];
    dispatch(setCategories(categories));
  });
};

export const getRecordTypes = () => async (dispatch: Dispatch) => {
  return Axios.get('/options/recordTypes').then((response) => {
    const {data} = response;
    const categories = data as RecordType[];
    dispatch(setRecordTypes(categories));
  });
};

export const addAccount = (account: AccountBalance): ActionTypes => {
  return {
    type: ADD_ACCOUNT,
    payload: account,
  };
};

export const setAccounts = (accounts: AccountBalance[]): ActionTypes => {
  return {
    type: SET_ACCOUNTS,
    payload: accounts,
  };
};

export const createAccount = (account: Account) => async (
  dispatch: Dispatch,
  getState: () => AppState,
) => {
  const state = getState();
  const {userId} = state.Auth;
  account.userId = +userId;
  return Axios.post(`/users/${userId}/accounts`, account).then((response) => {
    const {data} = response;
    const newAccount = data as Account;
    dispatch(addAccount({...newAccount, balance: 0}));
  });
};

export const getAccounts = () => async (
  dispatch: Dispatch,
  getState: () => AppState,
) => {
  const state = getState();
  const {userId} = state.Auth;
  const filter = {
    include: [
      {
        relation: 'records',
        scope: {
          order: ['date DESC'],
        },
      },
    ],
  };
  return Axios.get(
    `/users/${userId}/accounts?filter=${encodeURIComponent(
      JSON.stringify(filter),
    )}`,
  ).then((response) => {
    const {data} = response;
    const accountsRecords = data as AccountRecords[];
    const accounts = accountsRecords.map(
      ({records, ...accountFields}): AccountBalance => {
        const balance = records ? records[0].balance : 0;
        return {...accountFields, balance};
      },
    );
    dispatch(setAccounts(accounts));
  });
};

export const updateBalance = (newBalance: BalanceUpdate): ActionTypes => {
  return {
    type: UPDATE_BALANCE,
    payload: newBalance,
  };
};

export const addRecord = (record: Record) => async (dispatch: Dispatch) => {
  return Axios.post('/records', record).then((response) => {
    const {data} = response;
    const {balance, accountId} = data;
    message.success('New record Added');
    dispatch(updateBalance({accountId, balance}));
  });
};
