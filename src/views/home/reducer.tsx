import {
  SET_IS_OPEN_ACCOUNT_MODAL,
  SET_IS_OPEN_EXPENSE_RECORD_MODAL,
  USER_LOG_OUT,
  ActionTypes,
  SET_CATEGORIES,
  SET_RECORD_TYPES,
  SET_ACCOUNTS,
  ADD_ACCOUNT,
  UPDATE_BALANCE,
} from '../../globals/actionsTypes';
import {Category, RecordType, AccountBalance} from '../../globals';

type HomeState = {
  isOpenAccountModal: boolean;
  isOpenExpenseRecordModal: boolean;
  categories: Category[];
  recordTypes: RecordType[];
  accounts: AccountBalance[];
};

const initialState: HomeState = {
  isOpenAccountModal: false,
  isOpenExpenseRecordModal: false,
  categories: [],
  recordTypes: [],
  accounts: [],
};

const reducer = (state = initialState, action: ActionTypes): HomeState => {
  switch (action.type) {
    case SET_IS_OPEN_ACCOUNT_MODAL:
      return {
        ...state,
        isOpenAccountModal: action.payload,
      };
    case SET_IS_OPEN_EXPENSE_RECORD_MODAL:
      return {
        ...state,
        isOpenExpenseRecordModal: action.payload,
      };
    case SET_CATEGORIES:
      return {
        ...state,
        categories: action.payload,
      };
    case SET_RECORD_TYPES:
      return {
        ...state,
        recordTypes: action.payload,
      };
    case SET_ACCOUNTS:
      return {
        ...state,
        accounts: action.payload,
      };
    case ADD_ACCOUNT:
      const {accounts} = state;
      return {
        ...state,
        accounts: [...accounts, action.payload],
      };
    case UPDATE_BALANCE:
      const {accounts: previousAccounts} = state;
      const storedAccounts = previousAccounts.filter(
        ({id}) => id !== action.payload.accountId,
      );
      const modifiedAccount = previousAccounts.find(
        ({id}) => id === action.payload.accountId,
      ) as AccountBalance;
      return {
        ...state,
        accounts: [
          {...modifiedAccount, balance: action.payload.balance},
          ...storedAccounts,
        ],
      };
    case USER_LOG_OUT:
      return initialState;
    default:
      return state;
  }
};

export default reducer;
