import React, {useEffect} from 'react';
import {AppState} from '../../store';
import {connect, ConnectedProps} from 'react-redux';
import {Button, Row, Col, Typography, Tooltip, Card} from 'antd';
import {PlusOutlined} from '@ant-design/icons';
import {
  setIsOpenAccountModal,
  setIsOpenExpenseRecordModal,
  getCategories,
  getRecordTypes,
  getAccounts,
} from './actions';
import AddAccount from './modal/acount';
import AddRecord from './modal/record';
import {grid} from '../../globals';

const {Paragraph} = Typography;

const mapStateToProps = (state: AppState) => ({
  accounts: state.Home.accounts,
  userId: state.Auth.userId,
});

const mapDispatchToProps = {
  setIsOpenAccountModal: setIsOpenAccountModal,
  setIsOpenExpenseRecordModal: setIsOpenExpenseRecordModal,
  getCategories: getCategories,
  getRecordTypes: getRecordTypes,
  getAccounts: getAccounts,
};

const connector = connect(mapStateToProps, mapDispatchToProps);

type Props = ConnectedProps<typeof connector>;

const Home = (props: Props) => {
  useEffect(() => {
    props.getCategories();
    props.getRecordTypes();
  }, []);

  useEffect(() => {
    if (props.userId) {
      props.getAccounts();
    }
  }, [props.userId]);

  const getAccountsCards = (): React.ReactNode => {
    return props.accounts.map(({description, id, name, balance}) => (
      <Col {...grid} key={id} className="account-card">
        <Card title={name} hoverable extra={balance}>
          <Paragraph ellipsis={{rows: 1, expandable: true, symbol: 'more'}}>
            {description}
          </Paragraph>
        </Card>
      </Col>
    ));
  };

  const getFabButton = (): React.ReactNode => {
    return (
      <div className="fab-button">
        <Tooltip title="Add Record" placement="bottom">
          <Button
            type="primary"
            shape="circle"
            size="large"
            icon={<PlusOutlined />}
            onClick={() => props.setIsOpenExpenseRecordModal(true)}
          />
        </Tooltip>
      </div>
    );
  };

  return (
    <div className="home">
      <Row gutter={16}>
        <Col {...grid}>
          <Tooltip title="Add Account" placement="bottom">
            <Button
              type="dashed"
              className="new-account"
              icon={<PlusOutlined />}
              onClick={() => props.setIsOpenAccountModal(true)}
            />
          </Tooltip>
        </Col>
        {getAccountsCards()}
      </Row>

      {props.accounts.length ? getFabButton() : <React.Fragment />}

      <AddAccount />
      <AddRecord />
    </div>
  );
};

export default connector(Home);
