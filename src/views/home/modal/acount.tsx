import React from 'react';
import {Modal, Form, Input, Button} from 'antd';
import {AppState} from '../../../store';
import {setIsOpenAccountModal, createAccount} from '../actions';
import {connect, ConnectedProps} from 'react-redux';
import {useForm} from 'antd/lib/form/Form';
import {Account} from '../../../globals';
import {Store} from 'antd/lib/form/interface';

const mapStateToProps = (state: AppState) => ({
  isOpenAccountModal: state.Home.isOpenAccountModal,
});

const mapDispatchToProps = {
  setIsOpenAccountModal: setIsOpenAccountModal,
  createAccount: createAccount,
};

const connector = connect(mapStateToProps, mapDispatchToProps);

type Props = ConnectedProps<typeof connector>;

const layout = {
  labelCol: {span: 8},
  wrapperCol: {span: 16},
};

const validateMessages = {
  required: '${label} is required!',
};

const AddAccount = (props: Props) => {
  const [form] = useForm();

  const handleOk = () => {
    form
      .validateFields()
      .then((formValues: Store) => {
        let account = formValues as Account;
        props.setIsOpenAccountModal(false);
        props.createAccount(account).then(() => {
          form.resetFields();
        });
      })
      .catch(() => {});
  };

  const handleCancel = () => {
    props.setIsOpenAccountModal(false);
  };

  return (
    <Modal
      title="Add Account"
      visible={props.isOpenAccountModal}
      onOk={handleOk}
      onCancel={handleCancel}
      footer={[
        <Button key="back" onClick={handleCancel}>
          Cancel
        </Button>,
        <Button key="submit" type="primary" onClick={handleOk}>
          Submit
        </Button>,
      ]}>
      <Form
        {...layout}
        form={form}
        name="nest-messages"
        validateMessages={validateMessages}>
        <Form.Item name="name" label="Name" rules={[{required: true}]}>
          <Input />
        </Form.Item>

        <Form.Item name="description" label="Description">
          <Input.TextArea />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default connector(AddAccount);
