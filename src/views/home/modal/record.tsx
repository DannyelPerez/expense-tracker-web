import React, {useState} from 'react';
import {
  Modal,
  Form,
  Button,
  Select,
  InputNumber,
  DatePicker,
  Radio,
} from 'antd';
import {AppState} from '../../../store';
import {setIsOpenExpenseRecordModal, addRecord} from '../actions';
import {connect, ConnectedProps} from 'react-redux';
import {useForm} from 'antd/lib/form/Form';
import {Store} from 'antd/lib/form/interface';
import {RadioChangeEvent} from 'antd/lib/radio';
import moment from 'moment';
import {Record} from '../../../globals';
const {Option} = Select;
const mapStateToProps = (state: AppState) => ({
  isOpenExpenseRecordModal: state.Home.isOpenExpenseRecordModal,
  categories: state.Home.categories,
  recordTypes: state.Home.recordTypes,
  accounts: state.Home.accounts,
});

const mapDispatchToProps = {
  setIsOpenExpenseRecordModal: setIsOpenExpenseRecordModal,
  addRecord: addRecord,
};

const connector = connect(mapStateToProps, mapDispatchToProps);

type Props = ConnectedProps<typeof connector>;

const layout = {
  labelCol: {span: 8},
  wrapperCol: {span: 16},
};

const validateMessages = {
  required: '${label} is required!',
  types: {
    number: '${label} is not a validate number!',
  },
};

interface recordForm extends Record {
  datetime: moment.Moment;
}

const AddRecord = (props: Props) => {
  const [recordType, setRecordType] = useState(2);
  const [form] = useForm();

  const handleOk = () => {
    form
      .validateFields()
      .then((formValues: Store) => {
        const {datetime, ...record} = formValues as recordForm;
        const newRecord = {
          ...record,
          date: datetime.format(),
          recordTypeId: recordType,
        };
        props.addRecord(newRecord).then(() => {
          form.resetFields();
          props.setIsOpenExpenseRecordModal(false);
        });
      })
      .catch(() => {});
  };

  const handleCancel = () => {
    props.setIsOpenExpenseRecordModal(false);
  };

  return (
    <Modal
      title="Add Record"
      visible={props.isOpenExpenseRecordModal}
      onOk={handleOk}
      onCancel={handleCancel}
      footer={[
        <Button key="back" onClick={handleCancel}>
          Cancel
        </Button>,
        <Button key="submit" type="primary" onClick={handleOk}>
          Submit
        </Button>,
      ]}>
      <Form
        {...layout}
        form={form}
        name="nest-messages"
        validateMessages={validateMessages}>
        <Form.Item>
          <Radio.Group
            onChange={(e: RadioChangeEvent) => setRecordType(e.target.value)}
            value={recordType}>
            {props.recordTypes.map(({id, description}) => (
              <Radio value={id} key={id}>
                {description}
              </Radio>
            ))}
          </Radio.Group>
        </Form.Item>
        <Form.Item
          name="value"
          label="Value"
          rules={[{required: true, type: 'number'}]}>
          <InputNumber />
        </Form.Item>
        <Form.Item
          name="categoryId"
          label="Category"
          rules={[{required: true}]}>
          <Select placeholder="Select a Category">
            {props.categories.map(({id, description}) => (
              <Option value={id} key={id}>
                {description}
              </Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item name="accountId" label="Account" rules={[{required: true}]}>
          <Select placeholder="Select an Account">
            {props.accounts.map(({id, name}) => (
              <Option value={id} key={id}>
                {name}
              </Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          name="datetime"
          label="Date"
          rules={[
            {type: 'object', required: true, message: 'Please select time!'},
          ]}>
          <DatePicker showTime format="MM-DD-YYYY HH:mm" />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default connector(AddRecord);
