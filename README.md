## Installation 

After cloning the repo you must run `npm install`, then you can run `npm start` and the application will be running by default in  http://localhost:3000.


## Project Structure

```bash
├───project
    └───src
        ├── config
        │   └── axiosConfig.tsx
        ├── globals
        │   └── actionsTypes
        │   └── index.tsx
        ├── layout
        │   ├── index.tsx
        ├── routes
        │   ├── index.tsx
        ├── store
        │   ├── rootReducers.tsx
        │   └── configureStore.tsx
        └── views
            ├── home
               ├── actions.tsx
               ├── index.tsx
               └── reducer.tsx
            
```

* In the src folder, App.tsx is the root component call it on index.js file when the app starts on localhost:3000.
* In the global folder, we save all shared types and interfaces. Also, we save the redux actions types. 
* In the layouts folder, we define our Web Site layout (header, content, footer).
* The Store folder, we have `configureStore.tss` where it is configured the redux-store. 
* In the views folder, we have the site views. Each view has the next structure: 
    
    * `actions.tsx` The actions that interact with Saga to make the async API calls. 
    * `reducer.tsx` the redux state reducer. 


## How To Use
* First, run the project with `npm run dev`
* Interact with the website on http://localhost:3000

## Third-Party Libs
* [Ant Design](https://ant.design/docs/react/introduce)
* [Axios](https://github.com/axios/axios)
* [Redux](https://es.redux.js.org/)
* [React-Router](https://reacttraining.com/react-router/web/guides/quick-start)

